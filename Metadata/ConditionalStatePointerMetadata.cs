using System;
using Rikka.ProcessMachine.Core.Services;

namespace Rikka.ProcessMachine.Core.Metadata
{
    public class ConditionalStatePointerMetadata : IStatePointerMetaData
    {
        public Guid From { get; set; }
        public Guid To { get; set; }
        public Func<IProcessContext, bool> Check { get; }
    }
}