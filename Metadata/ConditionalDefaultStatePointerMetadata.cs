using System;

namespace Rikka.ProcessMachine.Core.Metadata
{
    public class ConditionalDefaultStatePointerMetadata : IStatePointerMetaData
    {
        public Guid From { get; set; }
        public Guid To { get; set; }
    }
}