using System;

namespace Rikka.ProcessMachine.Core.Metadata
{
    public class ProcessStateMetadata
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public object StateParams { get; set; }
    }
}