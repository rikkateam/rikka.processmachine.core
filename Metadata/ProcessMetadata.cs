using System;
using System.Collections.Generic;

namespace Rikka.ProcessMachine.Core.Metadata
{
    public class ProcessMetadata
    {
        public string Name { get; set; }
        public Guid StartStateId { get; set; }
        public IDictionary<Guid, ProcessStateMetadata> States { get; set; }
        public IStatePointerMetaData[] Pointers { get; }
    }
}