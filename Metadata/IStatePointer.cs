using System;

namespace Rikka.ProcessMachine.Core.Metadata
{
    public interface IStatePointerMetaData
    {
        Guid From { get; set; }
        Guid To { get; set; }
    }
}