using System;

namespace Rikka.ProcessMachine.Core.Metadata
{
    public class DirectStatePointerMetadata : IStatePointerMetaData
    {
        public Guid From { get; set; }
        public Guid To { get; set; }
    }
}