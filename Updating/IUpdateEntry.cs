using System;

namespace Rikka.ProcessMachine.Core.Updating
{
    public interface IUpdateEntry
    {
        string ProcessName { get; set; }
        DateTime OriginalTimeStamp { get; set; }
        Guid ProcessId { get; set; }
        Guid StateId { get; set; }

        /// <summary>
        /// �� ��������
        /// </summary>
        Guid SubscribeId { get; set; }
    }
}