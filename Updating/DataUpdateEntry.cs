using System;

namespace Rikka.ProcessMachine.Core.Updating
{
    public class DataUpdateEntry : IUpdateEntry
    {
        public DataUpdateEntryType Type { get; set; }
        public object Data { get; set; }

        /// <summary>
        /// ����� � ������� ���� ������� ����������
        /// </summary>
        public DateTime OriginalTimeStamp { get; set; }

        /// <summary>
        /// ������� ������� ���������� �� ��� ����������
        /// </summary>
        public Guid ProcessId { get; set; }

        /// <summary>
        /// �� ��������
        /// </summary>
        public Guid SubscribeId { get; set; }

        public Guid StateId { get; set; }
        public string ProcessName { get; set; }
    }
}