using System;
using System.Threading.Tasks;

namespace Rikka.ProcessMachine.Core.Updating
{
    public interface ISubscriber
    {
        Task<Guid> WaitData(string entityName, DataUpdateEntryType type);
        Task<Guid> WaitService(string service, ServiceUpdateEntryType type);
    }
}