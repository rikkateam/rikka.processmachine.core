namespace Rikka.ProcessMachine.Core.Updating
{
    public enum DataUpdateEntryType
    {
        Insert,
        Update,
        Delete
    }
}