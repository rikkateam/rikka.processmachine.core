using System;
using System.Threading.Tasks;
using Rikka.ProcessMachine.Core.Contracts.ClientContracts;

namespace Rikka.ProcessMachine.Core.Updating
{
    public class Subscriber : ISubscriber
    {
        private readonly IProcessExternalService _processExternalService;
        public bool Subscibed { get; private set; }
        public Subscriber(IProcessExternalService processExternalService)
        {
            _processExternalService = processExternalService;
        }
        public async Task<Guid> WaitData(string entityName, DataUpdateEntryType type)
        {
            var result = await _processExternalService.WaitData(entityName, type);
            Subscibed = true;
            return result;
        }

        public async Task<Guid> WaitService(string service, ServiceUpdateEntryType type)
        {
            var result = await _processExternalService.WaitService(service, type);
            Subscibed = true;
            return result;
        }
    }
}