using System;

namespace Rikka.ProcessMachine.Core.Updating
{
    public class ServiceUpdateEntry : IUpdateEntry
    {
        public ServiceUpdateEntryType Type { get; set; }
        public object Message { get; set; }

        /// <summary>
        /// ����� � ������� ���� ������� ����������
        /// </summary>
        public DateTime OriginalTimeStamp { get; set; }

        /// <summary>
        /// ������� ������� ���������� �� ��� ����������
        /// </summary>
        public Guid ProcessId { get; set; }

        /// <summary>
        /// �� ��������
        /// </summary>
        public Guid SubscribeId { get; set; }

        public Guid StateId { get; set; }
        public string ProcessName { get; set; }
    }
}