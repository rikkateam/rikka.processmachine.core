using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rikka.ProcessMachine.Core.Contracts;
using Rikka.ProcessMachine.Core.Contracts.ClientContracts;
using Rikka.ProcessMachine.Core.Metadata;
using Rikka.ProcessMachine.Core.Services;
using Rikka.ProcessMachine.Core.States;
using Rikka.ProcessMachine.Core.Updating;

namespace Rikka.ProcessMachine.Core
{
    public interface IProcessExecutorCore
    {
        void RegisterState<T>(string name) where T : IProcessState;
        void Load(IEnumerable<ProcessMetadata> processMetadata);
        Task StartProcess(string name, Guid id, object parameters);
        Task EnvironmentUpdates(IUpdateEntry updateEntry);
    }

    public class ProcessExecutorCore : IProcessExecutorCore
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IProcessExternalService _processExternalService;
        private readonly ILogger<ProcessExecutorCore> _logger;
        private readonly IDictionary<string, Type> _stateMap;
        public ProcessExecutorCore(IServiceProvider serviceProvider, IProcessExternalService processExternalService,ILogger<ProcessExecutorCore> _logger)
        {
            _serviceProvider = serviceProvider;
            _processExternalService = processExternalService;
            this._logger = _logger;
            ProcessMetadatas = new Dictionary<string, ProcessMetadata>();
            _stateMap = new Dictionary<string, Type>();
            RegisterBaseStates();
        }

        private void RegisterBaseStates()
        {
            RegisterState<StartState>("start");
            RegisterState<EndState>("end");
        }

        public void RegisterState<T>(string name) where T : IProcessState
        {
            _stateMap.Add(name, typeof(T));
        }

        public readonly IDictionary<string, ProcessMetadata> ProcessMetadatas;

        public void Load(IEnumerable<ProcessMetadata> processMetadata)
        {
            foreach (var metadata in processMetadata)
            {
                ProcessMetadatas.Add(metadata.Name, metadata);
            }
        }

        private ProcessContext CreateContext(Guid id, object parameters)
        {
            var context = new ProcessContext(id, parameters);
            return context;
        }

        private IProcessState GetState(ProcessStateMetadata metadata, ProcessContext context)
        {
            var type = _stateMap[metadata.Type];
            var constructor = type.GetTypeInfo().GetConstructors().FirstOrDefault();
            var args = new List<object>();
            foreach (var param in constructor.GetParameters())
            {
                var argument = param.ParameterType == typeof(IProcessContext)
                    ? context
                    : _serviceProvider.GetService(param.ParameterType);
                args.Add(argument);
            }
            var instance = Activator.CreateInstance(type, args.ToArray()) as IProcessState;
            return instance;
        }


        public async Task StartProcess(string name, Guid id, object parameters)
        {
            var process = ProcessMetadatas[name];
            await ExecProcess(name, id, process.StartStateId, parameters);
        }


        public async Task EnvironmentUpdates(IUpdateEntry updateEntry)
        {
            var parameters = await _processExternalService.GetProcessParameters(updateEntry.ProcessId);
            var context = CreateContext(updateEntry.ProcessId, parameters);
            var process = ProcessMetadatas[updateEntry.ProcessName];
            var stateMetadata = process.States[updateEntry.StateId];
            var state = GetState(stateMetadata, context);
            await ExecProcess(updateEntry.ProcessName, updateEntry.ProcessId, updateEntry.StateId, parameters,
                from: ProcessStatePoint.Update, updateEntry: updateEntry);
        }

        private async Task ExecProcess(string name, Guid id, Guid stateId, object parameters, 
            ProcessStatePoint from = ProcessStatePoint.Enter, IUpdateEntry updateEntry = null)
        {
            var context = CreateContext(id, parameters);
            var process = ProcessMetadatas[name];
            var currentState = stateId;
            var currentPoint = from;

            while (true)
            {
                var stateMetadata = process.States[currentState];
                var state = GetState(stateMetadata, context);

                while (true)
                {
                    var pointResult = await ExecStatePoint(id, currentState, context, state, currentPoint, updateEntry: updateEntry);
                    if (!pointResult)
                        return;
                    if (currentPoint == ProcessStatePoint.Leave)
                        break;
                    currentPoint += 1;
                }

                if (stateMetadata.Type == "end")
                {
                    await _processExternalService.CloseProcess(id, ProcessCloseReason.End);
                    return;
                }

                try
                {
                    currentState =  GetNextState(context, process.Pointers.Where(s => s.From == currentState).ToArray());
                }
                catch (Exception exception)
                {
                    _logger.LogCritical(1, exception, exception.Message);
                    await _processExternalService.UpdateProcessInfo(id, stateId, ProcessStatePoint.Error);
                    return;
                }
            }
        }

        private Guid GetNextState(ProcessContext context,IStatePointerMetaData[] pointers)
        {
            if (pointers.Length == 1 && pointers.First() is DirectStatePointerMetadata)
            {
                var pointer = (DirectStatePointerMetadata)pointers.First();
                return pointer.To;
            }

            if (pointers.Length >= 2 && !pointers.OfType<DirectStatePointerMetadata>().Any() &&
                     pointers.OfType<ConditionalDefaultStatePointerMetadata>().Count() <= 1 &&
                     pointers.OfType<ConditionalStatePointerMetadata>().Any())
            {
                var defaultPointer = pointers.OfType<ConditionalDefaultStatePointerMetadata>().FirstOrDefault();
                var conditions = pointers.OfType<ConditionalStatePointerMetadata>();
                foreach (var pointer in conditions)
                {
                    if (pointer.Check(context))
                    {
                        return pointer.To;
                    }
                }
                if (defaultPointer == null)
                    throw new Exception("Default pointer not declared");
                return defaultPointer.To;
            }
            throw new Exception("Wrong pointers combination");
        }

        private async Task<bool> ExecStatePoint(Guid id, Guid stateId, ProcessContext context, IProcessState state, ProcessStatePoint point, IUpdateEntry updateEntry = null)
        {
            var result = true;
            await _processExternalService.UpdateProcessInfo(id, stateId, point);
            try
            {
                switch (point)
                {
                    case ProcessStatePoint.Enter:
                        await state.Enter();
                        break;
                    case ProcessStatePoint.Execute:
                        await state.Execute();
                        break;
                    case ProcessStatePoint.ConfigureUpdate:
                        var subscriber = new Subscriber(_processExternalService);
                        await state.ConfigureUpdate(subscriber);
                        if (subscriber.Subscibed)
                            result = false;
                        break;
                    case ProcessStatePoint.Update:
                        result = await state.Update(updateEntry);
                        break;
                    case ProcessStatePoint.Leave:
                        await state.Leave();
                        break;
                }
            }
            catch (Exception exception)
            {
                _logger.LogCritical(1,exception,exception.Message);
                await _processExternalService.UpdateProcessInfo(id, stateId, ProcessStatePoint.Error);
                result = false;
            }
            if (context.ParametersChanged)
            {
                await _processExternalService.UpdateProcessParameters(id, context.Parameters);
                context.UpdateParametersHash();
            }

            return result;
        }

    }
}