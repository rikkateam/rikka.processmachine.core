namespace Rikka.ProcessMachine.Core.Contracts
{
    public enum ProcessStatePoint
    {
        Enter=1,
        Execute=2,
        ConfigureUpdate=3,
        Update=4,
        Leave=5,
        Error=100
    }
}