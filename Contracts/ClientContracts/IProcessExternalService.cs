using System;
using System.Threading.Tasks;
using Rikka.ProcessMachine.Core.Updating;

namespace Rikka.ProcessMachine.Core.Contracts.ClientContracts
{
    public interface IProcessExternalService
    {
        Task<Guid> WaitData(string entityName, DataUpdateEntryType type);
        Task<Guid> WaitService(string service, ServiceUpdateEntryType type);

        Task CloseProcess(Guid processId, ProcessCloseReason reason);
        Task UpdateProcessInfo(Guid processId, Guid stateId, ProcessStatePoint point);
        Task UpdateProcessParameters(Guid processId, object parameters);
        Task UpdateStateResult(Guid processId, Guid stateId, object result);
        Task<object> GetProcessParameters(Guid processId);
    }
}