using System.Threading.Tasks;
using Rikka.ProcessMachine.Core.Updating;

namespace Rikka.ProcessMachine.Core.Contracts
{
    public interface IProcessState
    {
        Task Enter();

        Task Execute();

        Task ConfigureUpdate(ISubscriber configureUpdateContext);

        Task<bool> Update(IUpdateEntry updateEntry);

        Task Leave();
    }
}