using System.Threading.Tasks;

namespace Rikka.ProcessMachine.Core.States
{
    public sealed class StartState : BaseState
    {
        public override async Task Execute()
        {
        }
    }
}