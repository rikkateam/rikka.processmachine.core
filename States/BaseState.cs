﻿using System.Threading.Tasks;
using Rikka.ProcessMachine.Core.Contracts;
using Rikka.ProcessMachine.Core.Updating;

namespace Rikka.ProcessMachine.Core.States
{
    public abstract class BaseState : IProcessState
    {
        public async Task Enter()
        {
        }

        public abstract Task Execute();

        public async Task ConfigureUpdate(ISubscriber configureUpdateContext)
        {
        }

        public async Task<bool> Update(IUpdateEntry updateEntry)
        {
            return true;
        }

        public async Task Leave()
        {
        }
    }
}
