using System.Threading.Tasks;

namespace Rikka.ProcessMachine.Core.States
{
    public sealed class EndState : BaseState
    {
        public override async Task Execute()
        {
        }
    }
}