using System;

namespace Rikka.ProcessMachine.Core.Services
{
    internal class ProcessContext : IProcessContext
    {
        public Guid Id { get; }
        public object Parameters { get; set; }
        private int _parametersHash;

        public ProcessContext(Guid id, object parameters)
        {
            Id = id;
            Parameters = parameters;
            _parametersHash = parameters?.GetHashCode() ?? 0;
        }

        public bool ParametersChanged => (Parameters?.GetHashCode() ?? 0) != _parametersHash;

        public void UpdateParametersHash()
        {
            _parametersHash = Parameters?.GetHashCode() ?? 0;
        }
    }
}