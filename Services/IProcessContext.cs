using System;

namespace Rikka.ProcessMachine.Core.Services
{
    public interface IProcessContext
    {
        Guid Id { get; }
        object Parameters { get; set; }
    }
}